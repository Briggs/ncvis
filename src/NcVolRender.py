import time
from vtk import *
def volumeRenderer(file):
    """ Renders a .vti file with opacity and colour transfers
    PARAMATERS:
        file: path to the file to be converted
    RETURNS:
        nothing

    NOTE: 
    """
    
    #CREATE THE VOLUME
    # Create transfer mapping scalar value to opacity
    opacityTransferFunction = vtkPiecewiseFunction()
    opacityTransferFunction.AddPoint(20, 0.0)
    opacityTransferFunction.AddPoint(255, 0.2)

    # Create transfer mapping scalar value to color
    colorTransferFunction = vtkColorTransferFunction()
    colorTransferFunction.AddRGBPoint(0.0, 0.0, 0.0, 1.0)
    colorTransferFunction.AddRGBPoint(128.0, 0.0, 1.0, 0.0)
    colorTransferFunction.AddRGBPoint(255.0, 1.0, 0.0, 0.0)

    # The property describes how the data will look
    volumeProperty = vtkVolumeProperty()
    volumeProperty.SetColor(colorTransferFunction)
    volumeProperty.SetScalarOpacity(opacityTransferFunction)
    volumeProperty.ShadeOn()
    volumeProperty.SetInterpolationTypeToLinear()

    # Create the reader for the data
    reader = vtkStructuredPointsReader()
    reader.SetFileName(file)

    # The mapper / ray cast function knows how to render the data
    volumeMapper = vtkGPUVolumeRayCastMapper()
    volumeMapper.SetBlendModeToComposite()
    volumeMapper.SetInputConnection(reader.GetOutputPort())

    # The volume holds the mapper and the property and
    # can be used to position/orient the volume
    volume = vtkVolume()
    volume.SetMapper(volumeMapper)
    volume.SetProperty(volumeProperty)

    #WINDOW STUFF
    # Create the standard renderer, render window and interactor
    renderer = vtkRenderer()
    renderWindow = vtkRenderWindow()
    renderWindow.AddRenderer(renderer)
    renderWindowInteractor = vtkRenderWindowInteractor()
    renderWindowInteractor.SetRenderWindow(renderWindow)

    renderer.AddVolume(volume)
    renderer.SetBackground(1, 1, 1)
    renderWindow.SetSize(600, 600)
    renderWindow.Render()

    def CheckAbort(obj, event):
        if obj.GetEventPending() != 0:
            obj.SetAbortRender(1)

    renderWindow.AddObserver("AbortCheckEvent", CheckAbort)

    renderWindowInteractor.Initialize()
    renderWindowInteractor.Start()