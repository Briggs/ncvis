import argparse
import sys
import os
from NcVolConverter import *
from NcVolRender import *

# Create parser
parser = argparse.ArgumentParser(description='Processes NetCDF4 and VTK data')

# Add arguments to argparse
parser.add_argument("-i", help="vtrPath to input file, for rendering or conversion")
parser.add_argument("-r", action="store_true", help="Render input file after conversion")
args = parser.parse_args()

if len(sys.argv) == 1:
    print("No arguments supplied...")
    parser.print_help()

# Check the provided file actually exists
try:
    file = open(args.i, 'r')
except FileNotFoundError:
    print("File not found, please check path: " + args.i)
    exit()

converter = NcVolConverter()
nc = vtr = vti = False
# Detect Input File
if args.i[-3:] == ".nc":
    nc = True
if args.i[-4:] == ".vtr":
    vtr = True
if args.i[-4:] == ".vti":
    vti = True

# Process file or render file
if nc == True:
    vtrPath = converter.netCDFToVTR(args.i)
    vtrPath = vtrPath[:-4] + ".vtr"
    vtiPath = converter.VTRToVTI(vtrPath)
    os.remove(vtrPath)
    if args.r == True:
        volumeRenderer(vtiPath)

if vtr == True:
    vtiPath = converter.VTRToVTI(args.i)
    if args.r == True:
        volumeRenderer(vtiPath)

if vti == True:
    volumeRenderer(args.i)