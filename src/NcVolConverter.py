from netCDF4 import Dataset
from pyevtk.hl import gridToVTK
import numpy as np
from vtk import *

class NcVolConverter:
    def netCDFToVTR(self, path):
        """ Converts netCDF4 to VTR Rectilinear Grid

        PARAMATERS:
            path: path to the file to be converted

        RETURNS:
            Full path to the saved file

        NOTE: 
        """
        # open netCDF file for reading.
        ncfile = Dataset(path,'r')

        # grab all keys in the file
        keys = []
        for key in ncfile.variables.keys():
            keys.append(key)

        # Copy data into numpy format from dataset
        x = np.array(ncfile.variables['xt'][:])
        y = np.array(ncfile.variables['yt'][:])
        z = np.array(ncfile.variables['zt'][:])

        # Copy additional data labels
        thermals = np.array(ncfile.variables['object_labels'][:])
    
        # Send all data to pyevtk library (also remove 3 letter extention from file)
        path = gridToVTK(path[:-3], x, y, z, pointData = {"thermals" : thermals})
        return path 

    def VTRToVTI(self, path):
        """ Converts Rectilinear Grid VTR file to Image VTI File

        PARAMATERS:
            path: path to the file to be converted
            
        RETURNS:
            Full path to the saved file

        NOTE: 
        """
    
        # read in VTR Rectilinear grid file
        vtrReader = vtkXMLRectilinearGridReader()
        vtrReader.SetFileName(path)
        vtrReader.Update()


        grid = vtrReader.GetOutput()

        # setup image file object
        image = vtkStructuredPoints()
        image.SetSpacing(1,1,1)
        image.SetExtent(grid.GetExtent())

        # import scalars to image object
        image.AllocateScalars(VTK_FLOAT, 1)
        scalars = grid.GetPointData().GetArray(0)
        image.GetPointData().SetScalars(scalars)

        # write file out
        writer = vtkStructuredPointsWriter()
        path = path[:-4] + ".vti"
        writer.SetFileName(path)
        writer.SetInputData(image)
        writer.Write()
        return path