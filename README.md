# NcVol

NcVol is a python library for volume rendering NetCDF4 '.nc' files. It also functions as a tool to convert a '.nc' file to a '.vti' VTK file.

## Installation

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install the following packages:

```bash
pip install numpy
pip install vtk
pip install netcdf4
pip install pyevtk.hl
```

*Setting up a virtual enviroment is highly reccomended.*

## Usage
There are two flags the program accepts.  
**-i:** Path to the input file  
**-r:** Tells program to immidately render if the input file is a '.nc' or '.vtr'. If the file is already '.vti' the program immidately renders.  

### Example conversion:  
**Command:**  
'python NcVol.py -i ./filepath/file.nc'  
**Output:**  
./filepath/file.vti - outputs vti file  

### Example rendering:
**Command:**  
'python NcVol.py -i ./filepath/file.nc -r'  
**Output:**  
./filepath/file.vti - outputs vti file

**Command:**  
'python NcVol.py -i ./filepath/file.vti'  
**Output:**  
renders file  
## Contributing  
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.\\

## License
MIT License
Copyright (c) [2019] [Zachary Briggs]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.